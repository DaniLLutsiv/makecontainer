const childProcess = require('child_process')


/*  default  /
function execProcess(command) {
    childProcess.exec(command, function (error, stdout, stderr) {
        console.log("stdout ", stdout)
        console.log("stderr ", stderr)
    })
}


/*  over size  */
function spawnProcess(command, args){
    const s_process = childProcess.spawn(command,args)
    let fullData = ''
    let dataChunks = 0

    /*  if error  */
    s_process.stderr.on('data', (data) => {
        console.log('stderr: ',data)
    })

    /* one piece */
    s_process.stdout.on('data', (data) => {
        fullData += data
        dataChunks++
        console.log('stdout ', data)
    })

    /*  end  */
    s_process.stdout.on('end', () => {
        console.log('end ',fullData)
        console.log('chunks ',dataChunks)
    })

    /*  close  */
    s_process.on('close', (code) => {
        console.log('code ',code)
    })
}

//execProcess('node -v')
//spawnProcess('node', ['-v'])
spawnProcess('curl',['https://en.wikipedia.org/wiki/List_of_2017_albums'])

