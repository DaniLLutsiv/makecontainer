const fs = require('fs'),
    path = require('path'),
    http = require('http')
const server = http.createServer((req,res) => {
  let filepath = path.join(__dirname, './code/index.html')

  fs.readFile('./index.html', (err, data) => {
    if (err){
      res.writeHead(500)
      res.end("ERR")
    }
    res.writeHead(200, {
      'Content_Type': 'text/html'
    })
    res.end(data)
  })
})

server.listen(3001, () => {
  console.log('Server starting...')
})