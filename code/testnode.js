const fs = require('fs')
/*fs.open('./text.txt', 'r', (err, fd) => {
  console.log(fd)
})
fs.stat('./text.txt', (err, stats) => {
  if (err) {
    console.error(err)
    return
  }
  console.log(stats)
})*/

/*fs.writeFile('./text.txt', 'new text', (err) => {
  if (err) {
    console.error(err)
    return
  }
  //write file
})
fs.readFile('./text.txt', 'UTF-8', (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  console.log(data)//read file
})*/


const path = require('path')
console.log(path.basename('/test/something')) //something
console.log(path.basename('/test/something.txt')) //something.txt
console.log(path.basename('/test/something.txt', '.txt')) //something
console.log(path.dirname('/test/something/test/test.txt')) // test/something/test
console.log(path.extname('/test/something/file.txt')) // '.txt'
console.log(path.relative('/Users', '/Users/flavio/test.txt')) //'flavio/test.txt'

/*
const EventEmitter = require('events')

const Emmit = new EventEmitter();

Emmit.on('some_event', function(request){
  console.log('some_event : ', request.from)
})

Emmit.emit('some_event', {from: "Client"});
*/


/*
fs.appendFile('text.txt', ' end, goodbye', (err) => {
  if (err) {
    console.error(err)
    return
  }
})
fs.readFile('./text.txt', 'UTF-8', (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  console.log(data)//read file
})
*/



// const {StringDecoder} = require('string_decoder')
// const decoder = new StringDecoder('utf8')
//
// textInBuffer = decoder.write(Buffer.from('blablabla'))
// console.log(decoder.end(Buffer.from(textInBuffer)));